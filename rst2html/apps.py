from django.apps import AppConfig


class Rst2HtmlConfig(AppConfig):
    name = 'rst2html'
