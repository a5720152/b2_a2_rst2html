from django.conf.urls import url

from . import views

app_name = 'rst2html'
urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^Pview/$', views.Preview, name='Preview'),
    url(r'^save/$', views.save, name='save'),
    url(r'^uploadImagePage/$', views.uploadImagePage, name='uploadImagePage'),
    url(r'^uploadImage/$', views.uploadImage, name='uploadImage'),
    url(r'^$', views.clearUpload, name='clearUpload'),
]
