from django.shortcuts import render,HttpResponse,HttpResponseRedirect
import os
import sys
import datetime
import zipfile
from django.views.static import serve
from django.core.urlresolvers import reverse
from django.core.files.storage import default_storage
def home(request):


    dirPath = 'rst2html/static/rst2html/uploadImage/'
    context = {}
    imagelist = []
    for file in os.listdir(dirPath):
        filename,ext = os.path.splitext(file)
        if ext == '.jpg'or ext == '.png'or ext == '.gif':
            imagelist.append(filename+ext)
    context['imagelist'] = imagelist
    
    return render(request,'rst2html/workpage.html',context)

def clearUpload(request):
    
    dirPath = 'rst2html/static/rst2html/uploadImage/'
    imagelist = []
    for file in os.listdir(dirPath):
        filename,ext = os.path.splitext(file)
        os.remove('rst2html/static/rst2html/uploadImage/{}{}'.format(filename,ext))

    return render(request,'rst2html/workpage.html')
def Preview(request):
    textFile = open('rst2html/static/rst2html/inputText.rst','w')
    textFile.write(request.GET['myTextBox'])
    textFile.close()
    command = request.GET['style']
    os.system('rst2html5 '+command+' rst2html/static/rst2html/inputText.rst > rst2html/static/rst2html/output.html')

    htmlFile = open('rst2html/static/rst2html/{}.html'.format('output'), 'r')
    html_content = ""
    for item in htmlFile:
        html_content = html_content + item  # keep all string in html file
    htmlFile.close()
    os.remove('rst2html/static/rst2html/inputText.rst')  # remove rst file
    os.remove('rst2html/static/rst2html/{}.html'.format('output'))  # remove html file
    return HttpResponse(html_content)


def uploadImagePage(request):

    return render(request,'rst2html/uploadImagePage.html')

def uploadImage(request):
    file = request.FILES['pic']
    dirPath = 'rst2html/static/rst2html/uploadImage/'
    saveFilePath = default_storage.save(dirPath, file)
    os.rename(saveFilePath, '{}{}'.format(dirPath, file.name))

    return render(request,'rst2html/uploadImagePage.html')
    

def save(request):# remove html file
    SlideName = request.GET['SlideName']
    os.system('mkdir '+SlideName)
    textFile = open(SlideName+'/inputText.rst','w')
    textFile.write(request.GET['myTextBox'])
    textFile.close()
    command = request.GET['style']
    os.system('rst2html5 '+command+SlideName+'/inputText.rst > '+SlideName+'/output.html')
    with zipfile.ZipFile(SlideName+'.zip', 'w') as myzip:
        myzip.write(SlideName+'/inputText.rst')
        myzip.write(SlideName+'/output.html')
        os.system('rm -r '+SlideName)
    
    file_path  = SlideName+'.zip'

    response = HttpResponse(open(file_path, 'rb').read() , content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename='+file_path
    return response
